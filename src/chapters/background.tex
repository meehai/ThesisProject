\chapter{Background}
\label{chapter:background}

\qquad This chapter makes a general presentation about the main technologies used for this thesis. It aims to provide a basic understanding, by showing examples, snippets of codes and explanations. The first part will talk about the VPN technology, what it provides, what problem it solves and how it's mostly used by users. After understanding the VPN, the focus will be on how to write a network application, including a VPN client or server, using BSD sockets and explaining them with a basic example. Then, the focus will be on virtualization, why it's useful today in a world that's fully connected to the Internet and how it's one of the core features of the Cloud infrastructure. On the topic of virtualization, the Xen bare metal hypervisor will be briefly explained.
The last part will be about the lwIP TCP/IP stack and Unikernels including the MiniOS, the bread and butter of the paper, on top of which the VPN server is built.

\section{VPN Technology}
\label{sec:background-vpn}

\qquad VPN stands for Virtual Private Network and is a network technology that extends a private network, such as a LAN or a corporate intranet, over a public network, like the Internet. Users connected to the private network, will be seen as if they'd be physically connected, but using a slower connection, caused by the delay in routing and geographic distance.

VPN uses the client-server model, an architecture where the two entities, clients (service requesters) and server (service provider), are separated and have different, well defined roles. The server will act as the network virtual device, a switch or a router, and all the clients connect to him as local clients connect to a switch in physical ports. The server's job is to uniquely identify each connection, by its physical address or logical address and direct (forward) incoming packets to the correct destination, in case of a unicast message, or direct to all the connected clients, in case of a broadcast message. MAC address, also called physical address, is a hardware stored address that uniquely identifies a device, while the IP address, also called a logical address, is an assigned address that identifies both the client (and the network he belongs using the mask) to in the Internet or private network he's connected to. Aditionally, the server can forward incoming packets with destination outside of the network to one of the prefered gateways. This process is similar to the process of packet routing done by network routers, which define a series of routing rules (statically set or dinamically learned) typicially based on the destination logical address, which help the choosing of the correct next-hop (networking term that refers to the closest router or network device a packet must go to in the process of routing).

The VPN technology typcially offer data encryption, in all the points where the user could be authentified by a third party, listening over the wire. That means all the traffic from the user to the VPN server will be encrypted using a shared secret only known by the client and the server. That secret will usually be generated using an authentication mechanism, such as those presented in \labelindexref{State of the Art}{chapter:sota}. 

There are multiple use cases for a user to use a VPN service: \begin{itemize} \item authenticate from a remote location to an office or company intranet \item use local network services, such as Android Network Services\cite{Android-Network-Services} (Android based technology that allows users in the LAN to identify, publish and use local services), zeroconf services\cite{zeroconf} (technology that allows the distribution of named services over a local network) or even video games that use LAN access, like Hamachi, which is a VPN technology designed specifically to create virtual LANs for video games over the Internet. \item browse the Internet under server's identification, thus using the server as a proxy to circumvent geographical or political restrictions \end{itemize}
For the average user, the third option is by far the most used option, and most of the VPN services offer high reliability and performance for a monthly cost to browse the Internet annonymous with reduced risks of getting your data and traffic linked to your profile.

In order to access a VPN server, usually the user must use a software application, typically called a VPN client that connects to the desired server. This client is most of the time tied and related to the server, but there are a lot of inter compatible applications, like \labelindexref{SoftEther}{sec:sota-softether}. The client uniquely identifies the user and sometimes a generated encryption key, signed by the server will be provided as well for the authentication of the user. After a user is authenticated, on the local machine a virtual interface will be created, which will gather an IP address in the virtual LAN he's connected. Virtual interfaces can be of two types: TAP interfaces and TUN interfaces.

 TAP interfaces will simulate a Layer-2 OSI interface and will send Link Layer frames, such as physical source and destination (MAC) or VLAN information. They are useful when working with devices that do not share the same Layer-3 OSI (like printers). TUN interfaces will only simulate a Layer-3 OSI interface, and most of the times only support the IP protocol. Those are useful when connecting PCs and mobiles only, that are compatible with the same protocol, and will guarantee a better throughput due to the lack of Layer-2 information sent over the network, but the difference is usually negligible.

Once connected to the VPN, a user should be able to talk to other users as if they'd be connected physically to a local switch. To test the functionality of the network, one can use tools such as ping to test the network, connection speed and the other users' responses.

\section{Virtualization}
\label{sec:background-virtualization}
\qquad Hardware virtualization is a software technique that refers to the creation of Virtual Machines that act like a real computer. The VM will simulate the underlying hardware, creating a virtual CPU, RAM, GPU and so on. The VM can also simulate the software, such as running a separate Operating System different from the one used in the physical machine (if any). Any software run on this virtualized OS, called \textit{guest} will be completely separated from the software run on the physical OS, called \textit{host}.

The software that creates virtual machines is called a Hypervisor and generally there are two types of hypervisors: \begin{itemize} \item Type 1 hypervisor, also called bare-metal hypervisor, runs directly on the host machine and manages the physical hardware to the Virtual Machines that he spawns. Examples of such hypervisors are: Xen\cite{Xen}, Oracle VM Server\cite{Oracle-VM-Server}, Microsoft Hyper-V\cite{Microsoft-Hyper-V}. \item Type 2 hypervisor, also called hosted hypervisor, runs as a process of an Operating System. Examples of type-2 hypervisors are: VMWare WorkStation\cite{VM-Ware}, VirtualBox\cite{VirtualBox} and QEMU\cite{QEMU}. \end{itemize}

Hardware virtualization can also be divided into 3 categories: \begin{itemize} \item Full virtualization, where the VMs hardware is entirely simulated. This allows software to run unmodified, but will have performance penalties. \item Partial Virtualization, where only a part of the hardware is virtualized, while other parts use the physical hardware. \item Paravirtualization, where hardware is not virtualized at all, but software is executed in a isolated domain and programs may require modifications to run in this environment. \end{itemize}
Besides this, there also exists Hardware-assisted virtualization, where with the assistance of the CPU, some specific hardware can be virtualized in a more efficient way, to increase performance, such as PCI passthrough\cite{PCI-passthrough}.

The idea of hardware virtualization is very important in today's business world. In the early 2000s, the trend was to run your own server, or datacenter, depending on the needs, thus requiring careful observation and protection of the hardware, network connection, cooling off the devices and so on. Virtualization has allowed this trend to migrate towards hardware renting, which moves the hardware problem in the hands of the provider. This is called nowadays Cloud Computing and basically represents the industry in which remote servers, hosted over the Internet are acquired on demand, in any shape, size or dimension a person might want. It's profitable for both parties, the seller, generally a company which owns huge amounts of servers will offer a better price than you would get in managing your own server, and also keeps the renter away from micromanaging it. Cloud computing offers, generally, services on demand, that means a demander only pay for what he wants, only for how long he is using the computer and based on how intensive the work he's doing is (CPU power of network traffic).

Cloud computing has 3 service models: \begin{itemize}\item Infrastructure as a service (IaaS), in which the resource is the computing infrastructure, like Virtual Machines, servers, storage or load balancers.\item Platform as a service (PaaS), where the resouce provided is a computing platform, generally including an Operating System, such as databases, web servers or development tools.\item Software as a service (SaaS), represents the highest layer of the cloud computing service. The software itself is run over the Internet, in the Cloud, and clients access it using specialized clients. Examples of such software are e-mail services, online games or instant messaging.\end{itemize}

The corelation between Virtualization and Cloud computing is one of the key factors of their success. Due to the way Cloud Computing works, with services on demand, the managing of such a diverse range of request would be close to impossible. Cloud vendors offer virtualized hardware and software, according to the clients' needs, thus solving the issue with flexibility. It is important that the virtualization mechanism is an efficient one, because even the tiniest fluctuation in performance could cause massive losses in money and business.

There are a few well known Cloud Computing vendors, such as Amazon, with Amazon Web Services (AWS), Google, with Google Cloud Platform and Microsoft, with Microsoft Azure.

\subsection{Xen}
\label{sec:background-virtualization-xen}
\qquad Xen\cite{Xen} is a type-1 Hypervisor, created by the Xen Foundation under an open source licence. It provides services that allow multiple Operating Systems, dedicated and general purpose, to be run on the same hardware machine. It aims to provide a very low performance hit, compared to running the OS on real hardware, while also permitting an easy resource configuration, like adjusting CPU or memory to the need of the users. 

Xen works by having a dedicated host Operating System, called Dom0, that has special full privileges, like direct access to hardware, and provides drivers (disk, network stack) for all the guest Operating Systems, called DomU (unprivileged domanins). For a guest OS to have access to a driver, it must be made available by Dom0, through the Backend Driver (running on Dom0) towards the Frontend Driver (running on DomU). Without having a Dom0, Xen cannot work independently. The available Operating Systems with host capabilities are: Linux, Windows, Solaris, NetBSD and FreeBSD.

The hypervisor is available in multiple Cloud platforms, such as CloudStack or OpenStack, and AWS alone runs half a million Xen Instances on their platform. It supports all 3 hardware virtualizations, discussed in the previous section: Full virtualization, Partial Virtualization and Paravirtualization.

Once you have a virtual machine image, running it on Xen is extremely simple. You need to create a simple config file, as described here: http://xenbits.xen.org/docs/unstable/man/xl.cfg.5.html
One simple config file will is this:
\lstset{language=bash,caption=Xen Configuration file,label=lst:background-virtualization-xen-cfg_file}
\begin{lstlisting}
kernel = "/path/to/vm.img"
builder = 'hvm'
memory = 4096
vcpus = 4
name = "VM-Name"
vif = ['bridge=xenbr0']
disk = ['phy:/dev/vg0/windows,hda,w','file:/root/windows.iso,hdc:cdrom,r']
\end{lstlisting}

The path to image is specified in the kernel line, the type of virtualization is in the builder line (hvm - Full Virtualization, generic - Paravirtualization), memory is specified in bytes, vcpus represent the number of virtual CPUs allocated to the VM, name will be the ID of the VM as seen in the status command and disk specifies the physical support for the virtual disks of the VM, which can be both physical disks or local files. Finally, vif represents if a virtual interface will be created and what attributes it will have (in this case, it is bridged to Xen's xenbr0 interface, commonly to provide Internet access). 

Starting a VM is as simple as:
\lstset{language=bash,caption=Xen: Starting a VM,label=lst:background-virtualization-xen-start-vm}
\begin{lstlisting}
xl create /path/to/cfg_file.cfg && xl console VM-Name
# or
xl create -c /path/to/cfg_file.cfg
\end{lstlisting}

Checking the status of active VMs can be found using:
\lstset{language=bash,caption=Xen: Cheking status of VMs,label=lst:background-virtualization-xen-status}
\begin{lstlisting}
xl list
\end{lstlisting}

Closing a VM is done using the following command:
\lstset{language=bash,caption=Xen: Closing a VM,label=lst:background-virtualization-xen-stop-vm}
\begin{lstlisting}
xl shutdown VM-Name
\end{lstlisting}

\section{Unikernels}
\label{sec:background-unikernels}
\qquad Unikernels\cite{Unikernels} are virtual images for dedicated Operating systems, containing only a minimal set of libraries that are required to run a specified application. The application is built on top of the minimal OS and then will run on top of a hypervisor or hardware without the control of another general purpose OS. The concept evolved from an earlier concept, called Exokernels, that appeared in the 90s, where the applications and the Kernel would be run in the same address-space, but directly over the hardware. Unikernels have the same principle, but the OS is run in a virtualized domain, allowing a better containerization and security of data. Also, the debugging of applications that are run in Unikernel domains is easier, because usually the software can be run in a non-Unikernel domain, where users have access to debug tools and profilers. 

One advantage of such a method is the fact that the application will be the only process or task that runs on the OS, thus increasing performance due to the lack of context switches, virtual memory (allowing the application to have access to the phyisical memory), dual mode (kernel space and user space) and even the lack of a complex task scheduler. All these components are vital to a general purpose OS, but in the case of a Unikernel, due to the fact that only one task runs at a time, they can be considered unnecessary overhead, that can be eliminated.

Moreover, the OS is run in a virtualised environment, where the hypervisor will provide the required resources, such as CPU or memory, based on how much the software is consuming. Some hypervisors allow only static allocation of resources, while others permit a communication with the OS, for dynamic allocation or reduction. Aditionally, the OS will be run in a containerized space, reducing the potential of security breaches, such as executing arbitrary code from outside the application due to a software bug. The address space and CPU are independent from other VMs, running on the same hardware, and there is no way of communication, without having the Hypervisor as a third party.

Unikernels provide a small memory footprint, due to the limited amount of shared libraries and other software required to run alongside the application itself. Generally the size of the image can be in order of MBs or even KBs, depending on the chosen solution, compiler optimizations or other dependenices. Another advantage of this architecture is the low boot time, generally being faster than a normal OS, thus allowing the concept of VM hot swap, without a interruption of users' work flow.

Such an approach is usually used when building specialised applications and service oriented software, but are unfit where multiple users would want to use the resources at the same time, or where multiple processes or tasks might want to be run one alongside other. Generally, these problems can be solved by compiling multiple Unikernels and boot them independently, or using a general purpose OS.

Most unikernels have their development in high level languages, like OCaml, Java, Haskell, or C++, to provide easy access to writing software, instead of working with low level and hardware constructs. 
Some implementations of Unikernel platforms are: \begin{itemize} \item ClickOS, a modular router that is written in C++ and has a rule-based language to provide desired routing rules for the incoming packets. \item MirageOS, one of the earliest Unikernels. It has an OCaml compiler for the user applications that are run in the address space of the OS.\item Rump Kernels\cite{RumpKernels}, is another approach that, instead of using their own software, aims to integrate existing software, unmodified, such as NetBSD sockets. Another advantage of this approach is the ability to run in both virtualized environment and directly on the hardware. \item MiniOS, is the base of most of these existing Unikernels. It was created by the Xen organization for the purpose of enabling Unikernel development for their Hypervisor. The application described in this paper is built for the MiniOS Unikernel, using lwIP as the network stack. \end{itemize}

\subsection{MiniOS}
\label{sec:background-unikernels-minios}
\qquad MiniOS\cite{MiniOS} is a lightweight Operating System created for the purpose to enable the development of Unikernels for the Xen hypervisor. Its architecture provides the minimal requirements for any other vendor to create their own OS, offering the data structures and steps involved when booting a VM. 

Besides offering a basic skeleton for other vendors, it also allows building applications on top of it and allow them to be run as dedicated software in a virtualised container. It provides single space addressing, minimal libc support, but with the possibility include more standard libraries, in what is called a stubdom, or stub domain. 

MiniOS also supports a simple filesystem, from which you can read or write data. To enable this, you must set up a disk in your \labelindexref{Xen config file}{lst:background-virtualization-xen-cfg_file}. This can be used for multiple purposes, such as logging data, or sharing information with Dom0. 

The OS also supports a lightweight TCP/IP stack, called LightWeight TCP/IP (lwIP), version 1.3.2, that will be discussed in section \labelindexref{LWIP TCP/IP stack}{sec:background-lwip}. To enable it, MiniOS must be compiled defining the \textit{LWIPDIR} environment variable:
\lstset{language=bash,caption=Compiling MiniOS with lwIP stack,label=lst:background-unikernels-minios-compile_lwip}
\begin{lstlisting}
make LWIPDIR=/path/to/lwip/sources
\end{lstlisting}

Developing applications for MiniOS is done in the C programming language, and the sources are compiled statically alongside the OS. The project files are included in the MiniOS makefile, and inclusion of shared libraries is not possible. The application will be run as a separate thread, which must be started by overriding the \textit{app_main} function. The overriding is done by the compiler, that will call the application's function over the default one, because it is using a \_\_weak\_\_ attribute.

After compiling the VM image, the default size is about 300Kb with no library, and about 700Kb when using the lwIP stack. Also, these VM images can be compressed using default zip format which also lessens the size of the image. MiniOS offers a very fast boot time, by default of under 1 second, as well as a small memory footprint, due to the reduced size of the bootable image.

Concrete information about the VPN Server, such as boot time or memory usage per user, will be presented in \labelindexref{Implementation}{chapter:implementation} and \labelindexref{Testing}{chapter:testing} chapters.

\section{Network programming}
\label{sec:background-sockets}
Programming of network applications involves software applications that communicate with eachother or with dedicated devices over a computer network. These applications are usually classified on the Layer-7 of the OSI stack. Usually, the programming of network applications is done over the IP protocol, which is generally available in every PC. This allows communication over regular LANs as well as over the Internet, if both end points provide a public IP address. Using the IP protocol also allows applications to be tested locally before launching them for Internet usage, but that also can provide a challenge because of unexpected delays, usage or other situations that only happen over the Internet.

Communication may be done using a connection-oriented method, by using TCP, or using a connection-less method, by using UDP or Raw IP, depending on the type of application that is being developed. Both TCP and UDP, are Layer-4 OSI protocols and implement the concept of network ports, allowing the same IP address to be used for separate connections, differentiated by this number. They vary from 1 to 65535, and the first 1024 are considered reserved for important services. One application can be bound to one or more ports and also each new established connection or request will use a random generated port, if none is specified. 

TCP is usually the safest method of implementing a network application. The connection itself is established using the three-way handshake, which is a simple authentication mechanism: The connection initiator sends a SYN message, the listener sends back a SYN+ACK message and finally the client sends another ACK message, confirming the listener's response. The protocol ensures reliability of data, meaning that once a file or message is sent it is guaranteed that it will arrive on the other end, if no connection failure is met on the way. If the data arrives corrupted, the protocol will ask for a resend of the corrupted bytes. Another guarantee of the TCP protocol is the data ordering, which means that when sending a number of messages, they will arrive in the same order on the other end. If some messages arrive in the wrong order, they are stored and the missing ones will be requested again. One problem with the TCP protocol is that if a connection is stopped, after a few retries, it is considered unrecoverable. This can be problematic when using a high latency Internet connection, or being in low signal areas. It also means that roaming is not supported. Some well known applications built on top of the TCP protocol are: HTTP (port 80), HTTPS (port 443), FTP (port 21), SSH (port 22).

UDP is the other fairly used method of creating a communication between 2 network applications. Software built using this protocol usually require a lower reliablity than TCP applications, but also have weaker constraints. Once a message is sent, the protocol itself cannot guarantee its arival, this is usually be done at application level, implementing an acknowledgement protocol of sorts. UDP is a connection-less protocol, meaning that there is no saved state between the two peers that are communicating, each message represents a new instance of a session. Again, applications might need to save an internal state, in order to work accordingly. Some applications built on top of UDP are: DNS, DHCP, VoIP applications (like Skype) and video streaming applications (Youtube).

The most common abstractization of network communication is done through the concept of sockets. A socket is created by the OS, and represents an end point of communication between two applications. Sockets can be created for TCP communication, UDP or even RAW packets. The OS usually represents the socket with a handler, that can be identified with a unique id, commonly a number. Berkley sockets is an API mostly used in the C programming language for creating Network applications. They are represented by a file descriptor, similar to files and the operations are very common to the standard file operations, like read() for reading the data that's come from the network and send() to send data to the other end point of the socket. TCP sockets can be in three states:\begin{itemize}\item listening, where other sockets will connect to the bound TCP port and address and upon connection, a new socket will be created for that respective connection.\item active, meaning data is ready to be sent or data is ready to be received.\item inactive, where the socket is either not connected, or data cannot be sent/received. Similar, UDP sockets can be in the same states, but a listening socket won't create any new connection, rather to socket itself will be the communication point.\end{itemize}

In C, connecting to a TCP listening socket (refered as a server), sending and reading data, is done like this:
\lstset{language=c,caption=Connecting to a TCP server,label=lst:background-sockets-connect_tcp}
\begin{lstlisting}
	int socketId = socket(AF_INET, SOCK_STREAM, 0);
	int serverPort = 10000;
	char *serverAddress = "server-address";
	char buffer[BUFSIZE];
	struct sockaddr_in serverSocketAddress;

	serverSocketAddress.sin_family = AF_INET;
	serverSocketAddress.sin_port = htons(serverPort); 
	inet_pton(AF_INET, serverAddress, &serverSocketAddress.sin_addr)

	connect(socketId, (struct sockaddr *)&serverSocketAddress, sizeof(serverSocketAddress));
	write(socketId, "Message");
	read(socketId, buffer, BUFSIZE);
\end{lstlisting}

As we can see in this example the server address and port are specified in a char and integer format and the inet_pton function is called to set up the address in the internal representation form. After that, the connect function is called, and the socketId represents the local end-point of communication with the server. After the connection is set up, we can read and write data, as we would do with a regular file. Of course, this simple example doesn't check for errors, as a normal program should do, like when the server is offline or some other problems appear.

The server will have a similar task, but a new socket is created when the client connects:
\lstset{language=c,caption=Connecting to a TCP server,label=lst:background-sockets-accept_tcp}
\begin{lstlisting}
	int listeningSocketId = socket(AF_INET, SOCK_STREAM, 0);
	int connectionSocketId;
	struct sockaddr_in serverSocketAddress;
	char buffer[BUFSIZE];

	serverSocketAddress.sin_family = AF_INET;
	serverSocketAddress.sin_port = htons(serverPort); 
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	bind(listeningSocketId, (struct sockaddr*)&serverAddress, sizeof(serverAddress)); 
	connectionSocketId = accept(listeningSocketId, NULL, NULL); 
	read(connectionSocketId), buffer, BUFSIZE);
	write(connectionSocketId, "Hello to server", BUFSIZE);
\end{lstlisting}

In this case, the bind function is called to bind the socket to the local address on the serverPort port. Connections are done to this port, and they are accepted with the accept function. Upon accepting a new connection, a socket, here called connectionSocketId is created and represents the server end-point of the communication process. After that, we can read and write data to the socket, similar to the client part.

Similar steps are done with UDP sockets. Further read about network programming can be found here: \url{http://beej.us/guide/bgnet/output/html/multipage/index.html}

\section{lwIP TCP/IP stack}
\label{sec:background-lwip}

\qquad lwIP\cite{lwip} is a lightweight implementation of a TCP/IP stack, released under an open source licence and generally used by developers working on applications that require a simpler network stack than the one provided in general purpose Operating Systems, like Windows or Linux. At the time of writing this thesis, the current stable version is 1.4.1, with 2.0.0 being in beta release. The focus of the project is to reduce the resource usage while still maintaining a fully operational system. The project is mostly used in embedded systems, where the RAM and ROM usage is one of the important constraints, but it was also ported to many real time Operating Systems (RTOS), like FreeRTOS. Aditionally, the stack is also available for Linux and Windows, thus allowing easy testing and developing under a general purpose OS, where debuggers and other helpful tools are available, like Valgrind\cite{Valgrind}. Once the application is developed and ready to be tested in a real environment, the porting is done almost immediately, because the software is ready to be used with close to no changes needed. lwIP is also supported in MiniOS, and it's the network stack the VPN server uses. Further implementation details will be presented in the \labelindexref{Implementation}{chapter:implementation} chapter.

In order for a bare metal project or OS to support lwIP, it must implement some important features required in the core of lwIP, like the network driver, endianess, common data types, mailboxes, for message passing, timeouts. Threads are also required if the port is done on top of an existing OS, so the driver developer must link the internal lwIP threads to the OS threads.

lwIP supoorts out of the box a bunch of protocols: \begin{itemize} \item Layer-2 and Layer-3 OSI: ARP, IPv4, IPv6, ICMP and IGMP \item Layer-4 OSI: Both TCP and UDP \item Layer-5, Layer-6 and Layer-7: DHCP, AUTOIP, SNMP and PPP \end{itemize}

The configuration of the stack is done by editing one file, called lwipopts.h. You can tweak almost anything, like disabling socket support, the size of the internal buffer sizes, the maximum length of the internal packet queue. You can disable timeouts, built-in protocols (like DHCP, or even TCP/UDP), threads usage, memory allocation, memory alignment and so on. For best performance, it's recommended that this file is tweaked for each system and application until the best results are found.

The network stack supports 3 Application programming interfaces (APIs)\cite{lwip-apis}: \begin{itemize} \item Raw API, also called native API, is the lowest level of developing an application in lwIP. Applications that are run on bare metal, or those that want to achieve the best performance out of the stack should use this API. One problem with it, is that it was developed with only a single thread architecture in mind. All the processing is done on the main thread, so blocking in packet processing will result in an overall decrease in performance throughout the application. It supports TCP, UDP and raw IP processing. The programming is done by using callbacks for every important action. For example, when processing TCP requests, one might bind a pcb (protocol control block) on a local port using tcp_bind function, set up a listener pcb using the function tcp_listen. After that, requests will be handled by the callback specified in the tcp_accept function. When new connections arrive, the callback is called and you can specify another callback to be called when new packets arrive, using tcp_recv. Other callbacks may be specified, such as an error callback, a polling callback or even a callback that is called once the data was received by the other end after a send. \item Netconn API, is the second API used by the lwIP stack. It's an abstractization layer, built on top of Raw API. It was designed to make the use of the stack easier, providing a similar API to sockets, while also maintaining zero-copy functionality. It supports TCP, UDP and Raw API connections. In order to create a connection, a user must call the netconn_new, using the NECTCONN_UDP or NETCONN_TCP macro, for example. To receive a packet, one must call netconn_recv and to send packets, netconn_send. The lack of callbacks is the most important factor of this API. \item Sockets API, is the highest level API of the lwIP stack. It's built on top of the netconn API and its aim is to create an environment similar to BSD sockets, so porting of existing applications to be very easy or even done automatically. The API has support for the basic functions, like creating a socket, listening, connecting, writing and reading data. It also supports the select function, a single threaded asynchronous function that allows a server to listen to multiple connections at the same time, and only send/receive data from those that are ready at any given time. \end{itemize}

The lwIP stack receives packets in structures called pbufs, which represent a list of internal buffers of static size. The size of the buffers as well as the maximum amount of nodes in the list can be specified by editing the lwipopts.h file. Generally, after a packet was received, the application can choose to keep the representation in pbufs, or transfer the data to application local buffers, and then allow the stack to receive more packets at the same time. The netconn API uses the concept of netbufs structure, which is a structure build on top of pbufs. In order to send or receive data, a netbuf must be available. Functions like netbuf_copy or netbuf_ref can be used to transfer the data to local a memory buffer.

The VPN server presented in the next chapter uses the Raw API for both TCP and a netconn implementation, for the UDP version. In future the UDP version will also be ported to Raw API, but for the moment it is a decent version.
