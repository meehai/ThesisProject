\chapter{State of the Art}
\label{chapter:sota}

\qquad This chapter presents some of the most used technologies for VPN servers in companies and personal use. Their main aspects will be covered, like how one differs from the other, their target market or simplicity of use. VPNs are an important subject regarding security of data over the Internet, so it's important to understand what different solutions exist and what they bring to the table. 

\section{Open VPN}
\label{sec:sota-openvpn}

\qquad OpenVPN\cite{OpenVPN} is one the most used VPN service for personal use. As the name suggests, the project is written under an open source licence, allowing everybody to use and extend its features. The current version, at the time of writing this thesis, is 2.3. 

OpenVPN has both a server and a client software implementation, and also a number of other VPN services offer compatibility with the OpenVPN server. The Server is available for the 3 most relevant PC platforms: Windows, Linux and macOS, mobile platforms, like Android, as well as some other hardware devices, like routers or dedicated tools. Peers can use multiple authentication methods, such as: username/password, pre-shared keys (the keys are distributed by the two parties beforehand) or digital certificates (signing the validy of the keys beforehand), generally the last two being the most secure ones. Also, the server can release authentication certificates, for every client, using its digital signature and certificate authority.

Network wise, the clients can use both Layer-4 OSI protocols: TCP and UDP. Aditionally, IPv6 is supported as the protocol inside the VPN tunnel and applications can establish connections using IPv6 addresses.

Regarding security, OpenVPN uses TLS/SSL and has an ecryption of up to 256 bits. The SSL algorithm generates session keys at the authentication step, randomly or using a Diffie-Hellman\cite{Diffie-Hellman} exchange algorithm.

In order to generate keys for the clients, in the case of using certificate-signed key authentication, the standard practice is to use the build-ca tool, included in the openssl package. This will create a Certificate Authority (CA), that will be used to sign the keys for the clients. Similar tools are provided in order to generate the server key, and clients' key. Clients' keys must be protected and generally distributing them requires a secure channel or even physical distribution directly to the client.

OpenVPN protocol is implemented by many private companies, thanks to the compatibility with OpenVPN clients and other private client implementations. Also, the VPN Server implementation is used by many private VPN services who use it as their base software, thanks to the open source license, applying self made modifications for other core features, such as stronger encryption or better performance.

Generally, private entities will provide a platform built on top of OpenVPN, such as strategically placed servers around the world for low latency, performant servers and backups to ameliorate failures, protection against denial of service, good bandwith and transparency about data usage. 

\section{Cisco VPN}
\label{sec:sota-ciscovpn}
\qquad Cisco is one of the largest newtork companies, designing, manufacturing and selling network equipments all over the world. They provide a big variety of routers, for both companies and personal usage. One of these routers is the Cisco RV325 Dual Gigabit WAN VPN Router\cite{Cisco-RV325}, which is a great router for companies as well as private people seeking performance, security and reliability. The main difference between this and more affordable routers is usually the performance, bandwidth and the number of possible simultaneous connections. 

The RV325 VPN engine creates secured tunnels over unsecured networks, allowing transmission of critical data. It supports both Gateway to Gateway and Client to Gateway modes. Gateway to Gateway tunnel refers to a tunnel where both of parties are routers, typically one being an on-site router, while the other being an off-site router. This allows both networks behind the routers to interract as being inside the same LAN, connecting 2 or more offices, for example. The Client to Gateway tunnels are mostly used by teleworkers and business travelers, and allow remote users to connect to a remote network and be identified as a local user. Such a connection is usually done using a third-party VPN software, similar to OpenVPN, as described in the previous section.

The router supports an extended amount of configurations, depending on the needs of the owner. It supports up to 10 SSL tunnels for remote client access, 25 IPsec site-to-site tunnels and 25 IPsec VPN tunnels. 

Encryption of the data is done using DES (Data Encyrption Standard), 3DES (Triple Data Encryption Algorithm), or AES (Advanced Encryption Standard) 128, 192 or 256 and also supports Internet Key Exchange (IKE) for authentication using certificates, for IPsec communication. A practical comparison, can be found at this reference\cite{DES-3DES-AES}. 

IPsec, a Layer-3 OSI protocol, works by mutually authenticating the both parties and encrypting the payload using negociated keys at the beginning of the session. It is more lightweight than using a Layer-6 encryption over IP (SSL), but can be less available or less usable due to firewall restrictions.

Generally, using a hardware VPN server will ensure a better performance than using a software server, but the downside will be the cost, simplicity and transparency of installation or usage of propieratary protocols. Typically, these devices are used in companies that require the highest amount of protection, performance and scalability. This paper tries to prove that software based VPN servers can achieve similar performances to hardware ones, if correctly implemented and used on top of lightweight Operating Systems, such as MiniOS.

\section{Fortinet VPN}
\label{sec:sota-fortinet}
\qquad Fortinet is another well known company that provides both hardware and software solutions for a various amount of networking and security problems. Similar to Cisco, they are using a proprietary OS, called FortiOS \cite{FortiOS}, Fortinet's networking Operating System, that is supported by their network devices. One of the features of this OS is the VPN Server, which supports both SSL and IPsec technologies.

Their gateways usually include ASICs (Application-specific integrated circuit) and FortiOS will rely on these dedicated circuits for high performance routing and data encryption, if they are available on the running hardware.

Choosing between the IPsec VPN and the SSL VPN depends mostly on the type of connection the user needs. IPsec, operates at network layer (Layer-3 OSI), while SSL VPN runs on top of SSL, a presentation layer protocol (Layer-6 OSI). For Client to Gateway connectivity, Fortinet recommends SSL VPN, because the traffic will most likely not be blocked over public networks. In the case of IPsec, the traffic may be restricted, and it's recommended mostly in Gateway to Gateway connections, as defined in \labelindexref{Cisco terms}{sec:sota-ciscovpn}.

FortiOS IPsec VPN supports the most common encryption methods in the industry standards, such as DES, 3DES and AES (128, 192 and 256), and for authentication it uses the IKE protocol, similarly to Cisco routers.

FortiOS SSL VPN has usually a simpler configuration than an IPsec VPN, because it works on top of the SSL technology. For encryption and decryption of data, FortiOS usually will use, if existent, specialized hardware, called FortiASIC\cite{FortiASIC} to provide better performance than using software algorithms.
Since SSL is built on most web browsers, because HTTPS uses the same technology, Fortinet supports a so called Web Portal Mode, which is a clientless mode of providing secure remote access through a web portal. Users will add their pre-configured \textit{bookmarks}, which can be even SSH, telnet or file-sharing applications. Aditionally, it also supports the classic Tunnel mode, that creates a virtual interface on the client's device and requires a third-party software, such as FortiClient.

Fortinet is a well established company and their products are usually good. Choosing a hardware VPN over a software one has the same advantages and disadvantages as in Cisco's case. Also, choosing between the two companies will probably be based on one's preference and price offers, rather than performance or professionalism.

\section{Microsoft SSTP VPN}
\label{sec:sota-microsoft-sstp}
\qquad Microsoft provides a built-in VPN service for all their Windows platforms, after Windows Vista, called the Microsoft Secure Socket Tunneling Protocol (Microsoft SSTP VPN). In order to connect to the VPN server, a user will user the integrated client, which is available with the OS. Microsoft provides a simple GUI for connecting where a user generally only needs to provide the address of the server, a username and password (for authentication). 

Setting up a VPN Server is also relatively easy, the software is already installed in Windows Server 2008, 2008R2 and 2012. In order to start a VPN Server, the network administrator must set up special roles, such as Internet Information Services (IIS) and Remote Access (RAS). The server requires generating and installing a SSL certificate, similar to OpenVPN. After this step, usually the server can be started using the Routing and Remote Access Server Setup Wizard, which is a GUI for the Windows Server. At the final step, you'll select VPN remote access, the type of VPN is SSTP, the authentication method is MS-CHAP v2 and usually data encryption is set to enabled.

The SSTP provides a mechanism to transport Layer-2 OSI traffic, such as Point-to-Point Protocol (PPP) or Layer 2 Tunneling Protocol (L2TP), a Layer-2 OSI tunneling protocol used to support VPNs, through a SSL channel. Both the server and the clients are authenticated in the PPP phase using the Microsoft Challenge-Handshake Authentication Protocol (MS-CHAP)\cite{MS-CHAP}, based on the username/password combination, provided by the user.

The MS-CHAP is Microsoft's implementation of the CHAP algorithm, that is used to authenticate an user to an authenticating entity, such as an ISP or in our case a VPN Server. The last version of MS-CHAP is v2 and provides stronger security measures compared to the first one. Some of these features are the removal of passowrd changes while connected, two-way authentication to validate the identity of both sides, separate cryptographic keys for transmitted and received data and the generation of keys is based on both the password of the user and an arbitrary challenge string hashed using a Secure Hash Algorithm (SHA). The SHA\cite{SHA} is a family of hashing algorithms, used to encrypt data with one-way functions.

Both MS-CHAP as well as MS-CHAP v2 use a DES (40, 56 and 128-bit) encryption method for the session keys, which are considered weak and susceptible to brute-force attacks. Furthermore, weakenesses have been found in the algorithm to simplify the brute-force attacks more, thus making the VPN relatively unsecure compared to other solutions.

Third party implementations, such as SoftEther, which is presented in the next section, are compatible with the Microsoft's VPN server and can provide even better performance\cite{SoftEther-vs-others}. While the technology may seem of lower quality than the other ones, the market share of Windows platform for PCs and the accesibility of the VPN client makes this service one of the most used VPN software.

\section{SoftEther VPN}
\label{sec:sota-softether}
\qquad SoftEther\cite{SoftEther} is an open source software platform that implements both server and client VPN capabilities. It was done as an academic project for the University of Tsukuba, Japan. 

SoftEther VPN Server has multiple different compatbile implementations, such as their own protocol, a compatible L2TP over IPsec implementation, a MS-SSTP implementation and an OpenVPN implementation. That means, using OpenVPN client or Microsoft's built-in VPN, you can connect to a SoftEther VPN Server. Similarly, they you can connect from iPhones, Android phones, Windows Mobile phones and Mac OS platforms using the built in L2TP client. 

SoftEther is one of the most informative solutions, having lots of referals, statistics and general useful VPN related informations on their website.

Regarding specifications\cite{SoftEther-specifications}, the SoftEther VPN Server is divided into multiple Virtual Hubs, which are independent Layer-2 OSI virtual LANs, incapable of communicating with eachother. User authentication, access lists, trusted certificate lists are managed by a Virtual Hub, and changes in one will have no effect on the others. Virtual Hubs are created in the server's GUI and it's possible to designate a Virtual Hub administration password for remote management of the network.
It's possible to create up to 4096 Virtual Hubs, and each of these can support up to 10000 Users and Groups, 32k ACL entries, 65k MAC and IP entries and up to 128 Cascade Connections.

Similarly to the other implementations, the Server supports SSL certificates and can generate its own Self Signed Certificate using the server's GUI. The server supports both Gateway to Gateway and Client to Gateway connections.

For authentication, the server supports annonymous authentication, password or RADIUS\cite{RADIUS} authentication and X.509 certificates, using RSA encryption for the Public Key Infrastructure (PKI).

Besides the standard SSL VPN and IPsec compatbile VPN, SoftEther supports their own VPN over ICMP and VPN over DNS\cite{SoftEther-VPN_over_ICMP_DNS} implementations for very restricted networks that only permit such specific packets.

With all these features, it is quite obvious that this solution is a very complex one, with lots of technologies included and compatibility layers with a lot of existing products. The fact that it is available on multiple platforms, integrates well with existent solutions and provides a lot of documentation makes this solution a very attractive one.\\

\qquad After analyzing a few of the most popular VPN solutions, the obvious conclusion is that the domain is a very diversified one, varying from software to hardware implementations, from data link protocols to application protocols in the OSI stack and generally available on all important platforms, such as mobile, PC or other devices. When a user is choosing a VPN solution, it is important that he understands his needs. Is top notch performance a factor ? Is accessing the office from a fringe location a necesity ? How well does he values his privacy in regards to potential attackers ?

The chapter is ended leaving the following tool as a resource in choosing a VPN service, which compares some solutions that are on the market at the moment: \url{https://thatoneprivacysite.net/}
